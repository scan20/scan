Dioptra
====

Dioptra is a cost analysis web application developed and maintained by the SCAN consortium.

Installation steps
------------------

Obtain the code:

```console
git clone --recursive git@github.com:theirc-dioptra/dioptra.git
```

The Dioptra application has two supporting services:

- Web Application: [Code repository](https://github.com/theirc-dioptra/dioptra-service-web)
- Transaction data pipeline: [Code repository](https://github.com/theirc-dioptra/dioptra-service-transaction-pipeline)

The source code for these services are embedded into this repository as Git
submodules:

```console
$ git submodule status
46a582de61b5dd143c058ac5b831db0714ed26bf services/transaction-data-pipeline (0.3.8-1-g46a582d)
72ffef7bd26a619ffc19cc4682fd6d955fc4fced services/web-application/src (0.4.4-26-g72ffef7)
```

For usage instructions of the two services, refer to the README.md file at
the root of each service.

Remote environments
-------------------

See the file README-instance-administration.md in this repository for instructions
on managing remote environments.
