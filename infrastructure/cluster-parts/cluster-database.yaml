Description: Dioptra / cluster / database

Parameters:

  StackName:
    Description: An environment name that will be prefixed to resource names
    Type: String

  InstanceType:
    Description: The Amazon RDS database instance class.
    Type: String

  AllocatedStorage:
    Description: The amount of storage (in gibibytes) to allocate for the DB instance.
    Type: Number

  DatabaseMasterUsername:
    AllowedPattern: ^([a-zA-Z0-9]*)$
    Description: The Amazon RDS master username.
    ConstraintDescription: Must contain only alphanumeric characters and be at least 8 characters.
    MaxLength: 16
    MinLength: 1
    Type: String

  DatabaseMasterPassword:
    Description: The Amazon RDS master password.
    Type: AWS::SSM::Parameter::Value<String>

  Subnets:
    Description: Choose which subnets this database should be deployed to
    Type: List<AWS::EC2::Subnet::Id>

  SecurityGroup:
    Description: Select the Security Group to use for the database
    Type: AWS::EC2::SecurityGroup::Id

Resources:

  DatabaseParameterGroup:
    Type: AWS::RDS::DBParameterGroup
    Properties:
      Family: postgres11
      Description: Require ssl on Postgres connections
      Parameters:
        rds.force_ssl: '1'

  DatabaseInstance:
    Type: AWS::RDS::DBInstance
    DeletionPolicy: Delete
    Properties:
      DBParameterGroupName: !Ref DatabaseParameterGroup
      Engine: postgres
      EngineVersion: 11.4
      MultiAZ: false
      MasterUsername: !Ref DatabaseMasterUsername
      MasterUserPassword: !Ref DatabaseMasterPassword
      DBInstanceClass: !Ref InstanceType
      PreferredBackupWindow: '09:00-09:30'
      BackupRetentionPeriod: '30'
      PreferredMaintenanceWindow: 'sun:07:00-sun:08:00'
      DBSubnetGroupName: !Ref DataSubnetGroup
      VPCSecurityGroups:
        - !Ref SecurityGroup
      StorageType: gp2
      AllocatedStorage: !Ref AllocatedStorage

  DataSubnetGroup:
    Type: AWS::RDS::DBSubnetGroup
    Properties:
      DBSubnetGroupDescription: !Sub Database Subnet Group for ${StackName}
      SubnetIds: !Ref Subnets

Outputs:
  DatabaseInstance:
    Value: !Ref DatabaseInstance
  DataSubnetGroup:
    Value: !Ref DataSubnetGroup
  DatabaseEndpoint:
    Value: !GetAtt DatabaseInstance.Endpoint.Address
