import time
import boto3
import botocore
import json
import os
import signal
import logging
import urllib

ROOT_ACCOUNT_ID = os.environ['ROOT_ACCOUNT_ID']
NEW_ACCOUNT_NAME = os.environ['NEW_ACCOUNT_NAME']
ORGANIZATIONAL_ROOT = os.environ['ORGANIZATIONAL_ROOT']
ORGANIZATIONAL_UNIT_ID = os.environ['ORGANIZATIONAL_UNIT_ID']

logging.getLogger().setLevel(logging.INFO)

def create_account():
    organizations_client = boto3.client('organizations')

    # Account
    account_response = organizations_client.create_account(
        Email="martin+%s@ombuweb.com" % NEW_ACCOUNT_NAME,
        AccountName=NEW_ACCOUNT_NAME,
        IamUserAccessToBilling='DENY'
    )

    account_status_id = account_response['CreateAccountStatus']['Id']
    while True:
        create_account_status = organizations_client.describe_create_account_status(
            CreateAccountRequestId=account_status_id
        )
        current_state = create_account_status['CreateAccountStatus']['State']
        logging.info("Creating AWS account: %s" % current_state)
        if current_state == 'FAILED':
            raise Exception(
                "AWS account creation failed",
                create_account_status['CreateAccountStatus']['FailureReason']
            )
        if current_state != 'IN_PROGRESS':
            account_id = create_account_status['CreateAccountStatus']['AccountId']
            logging.info("Created AWS account %s" % account_id)
            break
        time.sleep(8)

    organizations_client.move_account(
        AccountId=account_id,
        SourceParentId=ORGANIZATIONAL_ROOT,
        DestinationParentId=ORGANIZATIONAL_UNIT_ID
    )
    logging.info("Moved account under organization unit %s" % ORGANIZATIONAL_UNIT_ID)
    return account_id

def assume_role(account_id, account_role="OrganizationAccountAccessRole"):
    sts_client = boto3.client('sts')
    role_arn = 'arn:aws:iam::' + account_id + ':role/' + account_role
    assuming_role = True
    assumed_role_response = None
    while assuming_role is True:
        try:
            assuming_role = False
            assumed_role_response = sts_client.assume_role(
                RoleArn=role_arn,
                RoleSessionName="NewAccountRole"
            )
        except botocore.exceptions.ClientError as e:
            assuming_role = True
            logging.warning(e)
            logging.warning("Retrying...")
            time.sleep(20)

    # From the response that contains the assumed role, get the temporary
    # credentials that can be used to make subsequent API calls
    logging.info("Assumed role %s" % role_arn)
    return assumed_role_response['Credentials']

def create_provisioner_role(credentials):

    iam_client = boto3.client(
        'iam',
        aws_access_key_id=credentials['AccessKeyId'],
        aws_secret_access_key=credentials['SecretAccessKey'],
        aws_session_token=credentials['SessionToken']
    )

    role_name = "%s_provisioner_role" % NEW_ACCOUNT_NAME

    trust_policy_document = json.dumps(
        {
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Effect": "Allow",
                    "Principal": {
                        "AWS": "arn:aws:iam::"+ROOT_ACCOUNT_ID+":root"
                    },
                    "Action": "sts:AssumeRole"
                }
            ]
        }
    )

    create_role_response = iam_client.create_role(
        RoleName=role_name,
        AssumeRolePolicyDocument=trust_policy_document,
        MaxSessionDuration=3600
    )
    provisioner_role_arn = create_role_response['Role']['Arn']
    logging.info("Created provisioner role %s" % provisioner_role_arn)

    iam_client = boto3.client(
        'iam',
        aws_access_key_id=credentials['AccessKeyId'],
        aws_secret_access_key=credentials['SecretAccessKey'],
        aws_session_token=credentials['SessionToken']
    )

    # Use an Amazon managed Policy
    iam_client.attach_role_policy(
        RoleName=role_name,
        PolicyArn='arn:aws:iam::aws:policy/AdministratorAccess'
    )
    # Or use a custom policy
    # iam_client.put_role_policy(
    #     RoleName=newrole,
    #     PolicyName='NewRolePolicy',
    #     PolicyDocument=newrolepolicy
    # )
    return provisioner_role_arn

def main(event, context = None):

    # Handle Create stack operation
    if event['RequestType'] == 'Create':
        logging.info(event)
        account_id = create_account()
        credentials = assume_role(account_id)
        provisioner_role_arn = create_provisioner_role(credentials)
        response_data = {
            'account_id': account_id,
            'provisioner_role_arn': provisioner_role_arn
        }
        if context:
            send_response(event, context, "SUCCESS", response_data)
    # Handle Update stack operation
    elif event['RequestType'] == 'Update':
        response_data = {
            "reason": "create_account does not support the stack update action",
        }
        send_response(event, context, "FAILED", response_data)

    # Handle Delete stack operation
    elif event['RequestType'] == 'Delete':
        response_data = {
            "reason": "create_account does not support the stack delete action",
        }
        send_response(event, context, "FAILED", response_data)

def send_response(event, context, response_status, response_data):
    """ Send a resource manipulation status response to CloudFormation """
    response_body = json.dumps({
        "Status": response_status,
        "Reason": "See the details in CloudWatch Log Stream: " + context.log_stream_name,
        "PhysicalResourceId": context.log_stream_name,
        "StackId": event['StackId'],
        "RequestId": event['RequestId'],
        "LogicalResourceId": event['LogicalResourceId'],
        "Data": response_data
    }).encode('utf8')

    logging.info('Send response URL: %s', event['ResponseURL'])
    logging.info('Send response body: %s', response_body)

    req = urllib.request.Request(
        event['ResponseURL'],
        data=response_body,
        headers={'content-type': 'application/json'},
        method="PUT"
    )
    response = urllib.request.urlopen(req)
    logging.info("Send response status: %s", response.getcode())
    logging.info("Send response message: %s", response.msg)

if __name__ == "__main__":
    fake_lambda_event = {
        "RequestType": "Create",
    }
    main(fake_lambda_event)

def timeout_handler(_signal, _frame):
    """
    Handle SIGALRM
    """
    raise Exception('Time exceeded')

signal.signal(signal.SIGALRM, timeout_handler)
