## TODO

- [ ] Send FAILURE response to help CloudFormation stack fail gracefully
- [ ] Make AccountName a required parameter

## Testing `create_account.py` outside of Lambda

Test the create_account function standalone by:

    docker run -it --rm -v ${PWD}:/tmp -w /tmp \
    -e AWS_ACCESS_KEY_ID \
    -e AWS_SECRET_ACCESS_KEY \
    -e AWS_DEFAULT_REGION \
    -e AWS_SESSION_TOKEN \
    python:3-slim bash

Export the environment variables required by the script:

    pip install boto3 awscli
    export NEW_ACCOUNT_NAME="unique_account_name"
    export ROOT_ACCOUNT_ID="637354808552"
    export ORGANIZATIONAL_ROOT="r-93jh"
    export ="ou-93jh-yq0oi8ku"
    python create_account.py

A successful script run will result with a new AWS account with root email 
`martin+<NEW_ACCOUNT_NAME>@ombuweb.com`  provisioned under the ROOT_ACCOUNT_ID
withing the organizational unit ORGANIZATIONAL_UNIT_ID.

The new account will have an IAM role `<NEW_ACCOUNT_NAME>_provisioner_role`
that can be assumed from ROOT_ACCOUNT_ID:

    aws sts assume-role \
    --role-arn arn:aws:iam::<new account id>:role/<NEW_ACCOUNT_NAME>_provisioner_role \
    --role-session-name test_session`

After the test run, delete the new account.
