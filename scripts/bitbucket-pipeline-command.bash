#! /usr/bin/env bash
set -e

BOTO_VERSION=1.9.185

if [[ -z ${AWS_SECRET_ACCESS_KEY} || -z ${AWS_ACCESS_KEY_ID} || -z ${BUCKET_NAME} ]]
then
  echo "In order to upload the build artifact to S3, the Bitbucket repository must have the following variables defined:"
  echo "${_AWS_ACCESS_KEY_ID}, ${_AWS_SECRET_ACCESS_KEY}, ${_BUCKET_NAME}"
  echo "Define them in https://bitbucket.org/<repo>/admin/addon/admin/pipelines/repository-variables"
  exit 1
fi

# For the SemVer matching regex, see
# https://regex101.com/r/Ly7O1x/3/
echo "Build canidates are SemVer tags without the v prefix (i.e. 1.0.0, 1.0.0-dev1) and the tag latest"
is_build_candidate=$(if [[ $BITBUCKET_TAG == latest ]] || [[ $BITBUCKET_TAG =~ ^(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(-(0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(\.(0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*)?(\+[0-9a-zA-Z-]+(\.[0-9a-zA-Z-]+)*)?$ ]]; then echo "true"; fi)
if [[ $is_build_candidate ]]; then echo "'${BITBUCKET_TAG}' is a build candidate."; else echo "'${BITBUCKET_TAG}' is not a build candidate. Stopping."; fi
[[ -z $is_build_candidate ]] && exit 1

apt-get update -qq && apt-get install -y zip git
pip -q install boto3==${BOTO_VERSION}
echo "${BITBUCKET_TAG}" > VERSION
git submodule update --recursive --init
zip -r /tmp/application.zip * -x infrastructure/\*
python scripts/s3_upload.py ${BUCKET_NAME} /tmp/application.zip SourceArtifact/application.zip
