Dioptra Instance Administration
============================

Table of contents
----------------

- [Definitions](#definitions)
- [Using `stackedup`](#using-stackedup)
- [Launching a Dioptra instance](#launching-a-dioptra-instance)
- [Updating a Dioptra instance](#updating-a-dioptra-instance)
- [Deleting a Dioptra instance](#deleting-a-dioptra-instance)

* * *

Definitions
-----------

### Remote environments

The Dioptra application is designed to be hosted in Amazon Web Services (AWS).
When a Dioptra application is running on AWS, as opposed to running locally on a
developer's computer, it's considered to be running in a *remote environment*.

### Instances

A Dioptra application running in a remote environment is considered an *instance*.
The Dioptra project is designed to support running any number of instances in
remote environments. Examples of an instance could be a sandbox for the SCAN
consortium, or a production instance for an NGO.

### Clusters

Dioptra instances are launched in clusters. A *cluster* is a collection of AWS
resources, such as compute capacity, database, networking configuration, etc
that allows running one more instances.

### AWS accounts

A clusters is launched in an AWS account. An AWS account can hold any number of
clusters.

### Dioptra master account

Dioptra has a master AWS account that hosts the instances managed by the
consortium. Additional AWS accounts can be sub accounts managed and paid for by
SCAN, or independent AWS accounts managed and paid for by NGOs.

### Instance manifest

A structured file that describes the accounts, cluster and instance
configuration for all remote environments currently managed for Dioptra.

### Application images

The Dioptra software is distributed and deployed using Docker container images.
These images are stored in the Dioptra master AWS account. Because Dioptra software
is not publicly available, the Dioptra master AWS account must explicitly grant
access to the application images to other AWS accounts that will host
instances.

Using `stackedup`
----------------

Dioptra uses AWS CloudFormation to launch, update and delete accounts, clusters
and instances. Dioptra interacts with CloudFormation using a program called
`stackedup`, which relies of the instance manifest to help launch, update and
delete accounts, clusters and instances.

To use `stackedup`, first install it. Here we show how to do so in a Python
`virtualenv`:

```console
mkvirtualenv dioptra
pip install -r services/web-application/src/requirements/development.txt
```

Next, configure the shell environment with AWS access keys: an Access Key ID
and Secret Access Key your user in the Dioptra master account:

```console
export AWS_ACCESS_KEY_ID=<your-access-key-id>
export AWS_SECRET_ACCESS_KEY=<your-secret-access-key>
```

Dioptra requires multi-factor authentication (MFA) for interactions with remote
environments. Obtain AWS access keys, and a session token though MFA with this
commands:

```console
aws sts assume-role \
  --role-arn arn:aws:iam::<aws_account_id>:role/${role_name} \
  --role-session-name my_session \
  --serial-number <mfa_device_serial_number> \
  --token-code <token_code>
```

where:

- `aws_account_id` is the ID of the Dioptra master AWS account
- `role_name` is the name of the role to be assumed
- `mfa_device_serial_number` is the serial number of the virtual device used
  for MFA
- `token_code` is a current token code obtained from the virtual MFA device

The `assume-role` command will return AWS access keys for an MFA authenticated
session. Export the environment variables `AWS_ACCESS_KEY_ID`,
`AWS_SECRET_ACCESS_KEY`, `AWS_SESSION_TOKEN` for the session.

MFA authentication for AWS CLI is beyond the scope of this document. Please
refer to the AWS documentation for additional detail, such as [Authenticate Access
Using MFA Through the AWS
CLI](https://aws.amazon.com/premiumsupport/knowledge-center/authenticate-mfa-cli/)

Finally, confirm that you are able to use stacked up to interact with an
existing dioptra instance, gleaned from the instance manifest:

```console
cd infrastructure
stack-details application sandbox
```

This command will  output the details of the current application stack of the
`sandbox` instance.

The commands provided by `stackedup` are:

- `stack-details`: Describe a stack managed by `stackedup`
- `stack-launch`: Launch a stack defined in the instance manifest
- `stack-update`: Update a previously launched stack defined in the instance
                  manifest
- `container-shell`: Open a shell session in a remote environment
- `assume-role`: Assume an IAM role in a dioptra-managed account

`stackedup` commands need access to the instance manifest. By default,
`stackedup` looks for the `config.yaml` file in the current working directory.
This file exists in the `infrastructure` directory of the Dioptra source code.
`stackedup` commands must be ran from this directory, or they must be passed
the `--config` argument, pointing the command to the location of the instance
manifest.

Launching a Dioptra instance
-------------------------

When launching an instance, the developer must consider:

- What AWS account should host the instance. Generally, each NGO using the
  software should use a dedicated AWS account.
- What cluster within the selected AWS account should host the instance.  While
  a cluster can host multiple instances, it's recommended to use different
  cluster for testing and production instances.

Instances are launched within cluster, which exists in an AWS account. So the
first steps to launching an instance are to create the AWS and a cluster for it.
Often, instances are launched in existing AWS accounts or clusters. When
launching an instance in an existing AWS account and cluster, skip the next two
sections.

### Create a Dioptra-managed AWS account (optional)

1. Choose a unique name for the new account (`<name>`), such as `test`, and add
   an entry for it in the accounts section of the instance manifest:

    ```yaml test:
      parameters:
        AccountName: test
        OrganizationalUnitId: "ou-93jh-yq0oi8ku"
        OrganizationalRoot: "r-93jh"
    ```

    The values for `OrganizationalUnitId` , `OrganizationalRoot` parameters make
    this account managed by the parent Dioptra AWS account.

1. Create the account with `stack-launch account <name>`

1. Record the stack name in the instance manifest

    ```yaml
    test:
      stack_name: <ADD THE STACK NAME HERE>
      parameters:
        AccountName: test
        OrganizationalUnitId: "ou-93jh-yq0oi8ku"
        OrganizationalRoot: "r-93jh"
    ```

1. When the stack launch completes, inspect the stack outputs and record the 
   account ID and the provisioner role in the instance manifest:
   `stack-details account <name>`

    ```yaml
    test:
      stack_name: account-test-..... 
      id: <ADD THE ACCOUNT ID HERE>
      provisioner_role_arn: <ADD THE ROLE ARN HERE>
      parameters:
        AccountName: test
        OrganizationalUnitId: "ou-93jh-yq0oi8ku"
        OrganizationalRoot: "r-93jh"
    ```

1. In the Dioptra master AWS account, in Amazon Elastic Container Registry, update
   the permissions policy of **all** application image repositories with a
   principal entry for the new account:

```JSON
{
  "Statement": [
    {
      "Action": [
        "ecr:BatchCheckLayerAvailability",
        "ecr:BatchGetImage",
        "ecr:GetDownloadUrlForLayer"
      ],
      "Principal": {
        "AWS": [
          "arn:aws:iam::<id-of-existing-account-1>:root",
          "arn:aws:iam::<id-of-existing-account-2>:root",
          "arn:aws:iam::<ADD THE ACCOUT ID HERE, ON THIS NEW LINE>:root"
        ]
      },
      "Effect": "Allow",
      "Sid": "AllowPull"
    }
  ],
  "Version": "2008-10-17"
}
```

### Create an instance cluster (optional)

1. Choose a region for the cluster. Clusters for Dioptra instances are located in
   a specific region in AWS, such as `us-west-2`.

1. If the cluster is in a new AWS account, create a private S3 bucket in the
   desired region `stackedup` will use to package stack templates. Record its
   name in the instance manifest under the account:

    ```yaml
    test:
      stack_name: dioptra-account....
      id: 123...
    cloudformation_bucket: <ADD THE BUCKET NAME HERE>
      provisioner_role_arn: arn:aws:iam:....
      parameters:
        AccountName: test
        OrganizationalUnitId: "ou-93jh-yq0oi8ku"
        OrganizationalRoot: "r-93jh"
    ```

1. Create the dependencies for the cluster in the desired region:

    1. Log into the new account AWS console, by assuming the provisioner role
       from Dioptra's root account. For more details on this, see the AWS
        documentation [Switching to a Role (Console)](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_use_switch-role-console.html)

    1. In EC2, create or import a Key Pair and note the name and securely save the
       private key (if you had EC2 generate the the key pair)

    1. In Amazon Simple Email Service, under Sending Statistics, submit a
       request to AWS Support requesting a Sending Limit Increase, to remove
       the account from its default *sandbox* access.

    1. In Amazon Simple Email Service, verify any domains or email addresses
       that will be used to send email from, from this account

    1. In AWS Certificate Manager (ACM), add a TLS certificate covering the
       domain(s) of any instances that will be hosted in the account, and take
       note the certificate ARN. This will be a required parameter to launch a
       stack.

1. Launch an instance cluster

    1. Choose a cluster name (`<name>`), such as `test-cluster`, and add an
       entry for it in the clusters section of the instance manifest:

       ```yaml
       test-cluster:
         account: test
         region: us-west-2
         parameters:
           KeyName: <they key name>
           CertificateArn: <the certificate arn>
           InstanceType: t3.small
           ClusterSize: "1"
           DbAllocatedStorage: "5"
           DbInstanceType: db.t3.small
       ```

       Where:

           - account is the desired account name for the cluster
           - region is the selected region for the cluster
           - KeyName is the name of they key created previously
           - CertificateArn is ARN of the certificate created previously
           - InstanceType and DBIntanceType declare the desired server
             sizes for the instance
           - ClusterSize: declares the number of servers to support the
             container cluster
           - DBAllocatedStorate declares the storage size in GB for the
             database

    1. In AWS Systems Manager Parameter store, create the following parameters
       with a *String* type. The values should for each should be a different,
       random, secure password:

           - `/dioptra/test-cluster/rdsmaster_password`
           
    1. Create the cluster with `stack-launch cluster <name>`. This step will
       take approximately 15 minutes, as AWS provisions the resources to run
       Dioptra instances.

    1. Record the stack in the cluster entry in the instance manifest:

    ```yaml
    test-cluster:
      stack_name: <ADD THE STACK NAME HERE>
      account: test
      region: us-west-2
      parameters:
        KeyName: <they key name>
        CertificateArn: <the certificate arn>
        InstanceType: t3.small
        ClusterSize: "1"
        DbAllocatedStorage: "5"
        DbInstanceType: db.t3.small
    ```

    1. Inspect the cluster outputs with:
        `stack-outputs cluster <name>`

### Launch the instance

1. Choose a unique name for the instance (`<name>`), such as `test-instance`,
   and add an entry for it in the instances section of the instance manifest.

   The instance entry has a section for each micro service that supports the
   instance, such as the application, the transaction pipeline and, optionally,
   the sftp, etc.

    ```yaml
      test-instance:
        account: dioptra-ombutest2
        cluster: test-cluster
        transaction-pipeline:
          parameters:
            ClusterStack: test-cluster-....
            ECRRepository: 637354808552.dkr.ecr.us-west-2.amazonaws.com
            InstanceName: test-instance
            FromEmailAddress: test-instance@example.org
            SentryDsn: "https://123...@sentry.io/123..."
            SESIdentityArn: ...
            Version: "1.0"
        application:
          parameters:
            ClusterStack: test-cluster-....
            Domain: test-instance.example.org
            ECRRepository: 637354808552.dkr.ecr.us-west-2.amazonaws.com
            EnvironmentType: production
            FromEmailAddress: test-instance@example.org
            InstanceName: test-instance
            ListernerPriority: "1101"
            SentryDsn: "https://123...@sentry.io/123..."
            TransactionPipelineStack: transaction-pipeline....
            Version: "1.0"
    ```

    where:

        - ClusterStack is the CloudFormation stack name of the cluster
        - ECRRepository is the hostname of the Docker container registry from
          the Dioptra master account
        - Version is the desired version for the service. This version must be
          an existing Docker tag in the service container registry
        - SentryDsn is a DSN from sentry.io where the application will log
          exceptions
        - ListernerPriorit is a unique integer.If there are multiple instances
          in the cluster, the ListenerPriority parameter must be unique for
          this instance
        - EnvironmentType is one of testing, staging, production
        - SESIdentityArn is the ARN of the SES identity (email address or
          domain) previously verified creating the instance cluster
        - A Sentry DSN where the instance will record exceptions is
           required

1. Prepare to launch the instance

    1. In AWS Systems Manager Parameter store, create the following parameters
       with a *String* type. The values should for each should be a different,
       random, secure password:

           - `/dioptra/<instance-name>/database_password`
           - `/dioptra/<instance-name>/transaction-pipeline-db-password`
           - `/dioptra/<instance-name>/django_secret_key`

    1. If the instance will use the Okta authentication provider, set these
       additional parameters:
       
           - `/dioptra/<instance-name>/okta_client_id`
           - `/dioptra/<instance-name>/okta_client_secret`
        
    1. Using the database endpoint name obtained from the cluster stack
       details, connect to the database server and create a database for the
       application service, with these SQL commands:

    ```SQL
    CREATE DATABASE "<instance-name>";
    CREATE USER "<instance-name>" WITH ENCRYPTED PASSWORD '<database-password>';
    GRANT ALL PRIVILEGES ON DATABASE "<instance-name>" TO "<instance-name>";
    ```

    where `<database-password>` matches the value used in the parameter
    `/dioptra/<instance-name>/database_password`. Connecting to the database will
    require:

        - Tunneling through an EC2 instance in the cluster
        - Temporarily modifying the instance security group to allow external connections
          on SSH port 22
        - Obtaining master database credentials by viewing the cluster
          infrastructure manifest file `infrastructure/cluster.yaml`

1. Launch the instance services

    1. Launch the transaction pipeline:

        `stack-launch transaction-pipeline <name>`

    1. Note the transaction pipeline stack name and record it in the instance
       manifest.

    ```yaml
    transaction-pipeline:
      stack_name: <ADD THE STACK NAME HERE>
      parameters:
        ClusterStack: test-cluster-....
        ECRRepository: 637354808552.dkr.ecr.us-west-2.amazonaws.com
        InstanceName: test-instance
        FromEmailAddress: test-instance@example.org
        SentryDsn: "https://123...@sentry.io/123..."
        SESIdentityArn: ...
        Version: "1.0"
    ```

    and

    ```yaml
    application:
      parameters:
        ClusterStack: test-cluster-....
        Domain: test-instance.example.org
        ECRRepository: 637354808552.dkr.ecr.us-west-2.amazonaws.com
        EnvironmentType: production
        FromEmailAddress: test-instance@example.org
        InstanceName: test-instance
        ListernerPriority: "1101"
        SentryDsn: "https://123...@sentry.io/123..."
        TransactionPipelineStack: <ADD THE STACK NAME HERE>
        Version: "1.0"
    ```

    1. Launch the web application service with `stack-launch application <name>`

    1. Note the web application service stack name and record it in the
       instance manifest.

    ```yaml
    application:
      stack_name: <ADD THE STACK NAME HERE>
      parameters:
        ClusterStack: test-cluster-....
        Domain: test-instance.example.org
        ECRRepository: 637354808552.dkr.ecr.us-west-2.amazonaws.com
        EnvironmentType: production
        FromEmailAddress: test-instance@example.org
        InstanceName: test-instance
        ListernerPriority: "1101"
        SentryDsn: "https://123...@sentry.io/123..."
        TransactionPipelineStack: transaction-pipeline....
        Version: "1.0"
    ```

    1. Use the container-shell command to open a shell session in the
       application web service container `container-shell test-instance
       application Django django` and run this command to initialize the
       application:

            python manage.py build -y

    1. Visit the application at the configured domain

    1. Optionally, if the instance will use the SFTP shim, add it to the
       instance section of the manifest:

        ```yaml
        sftp:
          stack_name: <ADD THE STACK NAME HERE>
          parameters:
            PipelineStack: test-pipeline....
            SSHPublicKey: "ssh-rsa AAAA....."
            UserName: an-sftp-username
        ```

        where:

            - SSHPublicKey is the desired public part of the SSH key that will
              be used for SSH key-based authentication with the SFTP
            - UserName is the desired user name for the SFTP connection

    1. Launch the web application service with `stack-launch sftp <name>`

    1. Note the sftp service stack name and record it in the instance manifest.

        ```yaml
        sftp:
          parameters:
            PipelineStack: test-pipeline....
            SSHPublicKey: "ssh-rsa AAAA....."
            UserName: an-sftp-username
         ```

Updating a Dioptra instance
------------------------

Existing instances may be updated using the `stack-update` command, after
making the desired updates in the instance manifest. Because Dioptra instances are
a collection of micro services, services are updated independently with
`stack-update <service> <instance-name>`.

For example, to update the application service of an instance with name
*sandbox*, edit the service in the instance manifest with the desired change
and run:

```console
stack-update application sandbox
```

The run `stack-details` to view the status of the update:

```console
stack-update application sandbox
```

Additionally, clusters may also be updated using the stack-update command:

```console
stack-update cluster <name>
```

Deleting a Dioptra instance
------------------------

Deleting an instance is a destructive operation involving data loss. As a
result, `stackedup` does nor provide facilities to easily delete instances. To
delete an instance, obtain the stack names for the instance services from the
instance manifest and use AWS CloudFormation to delete the stacks.

Note that AWS will also attempt to protect from data loss, so certain instance
resources, such as S3 buckets will need to be manually emptied before
CloudFormation allows deleting the stack that manages them.

